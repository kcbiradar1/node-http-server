const fs = require("fs").promises;

const http = require("http");

const { v4: uuidv4 } = require("uuid");

const server = http.createServer(async (request, response) => {
  const not_found_message = `
        404 Not Found
        Please visit these paths,
        1. /html
        2. /json
        3. /uuid
        4. /status/{status_code}
        5. /delay/{number_of_seconds}
  `;
  try {
    const html_content = await fs.readFile("./public/index.html", "utf-8");
    const json_content = await fs.readFile("./public/data.json", "utf-8");
    const store_url = request.url.split("/");

    request.url = store_url[1];

    const genarate_uuid = uuidv4();
    switch (request.url) {
      case "html":
        response.write(html_content);
        response.end();
        break;
      case "json":
        response.write(json_content);
        response.end();
        break;
      case "uuid":
        response.write(
          JSON.stringify({
            uuid: genarate_uuid,
          })
        );
        response.end();
        break;
      case "status":
        let status_code = parseInt(store_url[2]);
        response.writeHead(status_code, {'Content-Type' : 'text/plain'});
        if (store_url.length >= 2) {
          response.write(
            JSON.stringify({
              status_code: store_url[2] || 200,
            })
          );
          response.end();
          break;
        }
        response.end(not_found_message);
        break;
      case "delay":
        setTimeout(() => {
          response.write(
            `Hello World from MountBlue after ${store_url[2]} seconds delay`
          );
          response.end();
        }, 1000 * parseInt(store_url[2]));
        break;
      default:
        response.end(not_found_message);
    }
  } catch (error) {
    console.error(error.message);
  }
});

server.listen(1221, () => {
  console.log("Server running...");
});
